﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class Motion : MonoBehaviour {
    public GameObject BodyManager;

    #region GameObjects: parts of body
    
    public GameObject Root;

    public GameObject Neck;
    public GameObject Head;

    public GameObject Hip;
    public GameObject LeftThigh;
    public GameObject LeftKnee;
    public GameObject LeftAnkle;
    public GameObject LeftToe;
    public GameObject RightThigh;
    public GameObject RightKnee;
    public GameObject RightAnkle;
    public GameObject RightToe;

    public GameObject Ribs;
    public GameObject LeftShoulderArm;
    public GameObject LeftUpperArm;
    public GameObject LeftForearm;
    public GameObject LeftWrist;
    public GameObject LeftIndexFinger;
    public GameObject RightShoulderArm;
    public GameObject RightUpperArm;
    public GameObject RightForearm;
    public GameObject RightWrist;
    public GameObject RightIndexFinger;

    
    #endregion

    BodySourceManager bodyManager;
    Body[] bodies;
    
    private Quaternion[] defaultJointsState;
    private Vector3 defaultRootPosition;

    void Start () {
        defaultJointsState = new Quaternion[50];
        defaultRootPosition = new Vector3();
        SetDefaultModelState();
    }

	void Update () {

        if (BodyManager == null)
            return;

        bodyManager = BodyManager.GetComponent<BodySourceManager>();
        if (bodyManager == null)
            return;

        bodies = bodyManager.GetData();
        if (bodies == null)
            return;


        Body body = null;
        for (int i = 0; i < bodies.Length; ++i)
            if (bodies[i] != null && bodies[i].IsTracked)
            {
                body = bodies[i];
                break;
            }

        if (body == null)
        {
            ResetCurrentModelState();
            return;
        }

        Head.transform.rotation = CountQuaternion(body.JointOrientations[JointType.Head])
             * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
        Neck.transform.rotation = CountQuaternion(body.JointOrientations[JointType.Neck])
             * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));

        Ribs.transform.rotation = CountQuaternion(body.JointOrientations[JointType.SpineShoulder])
             * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
        Hip.transform.rotation = CountQuaternion(body.JointOrientations[JointType.SpineBase])
             * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));

        Root.transform.rotation = CountQuaternion(body.JointOrientations[JointType.SpineMid])
             * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
        Root.transform.position = new Vector3(body.Joints[JointType.SpineMid].Position.X * 1.3f, body.Joints[JointType.SpineMid].Position.Y + 1.4f, 3 - body.Joints[JointType.SpineMid].Position.Z);

        #region Left arm

        LeftShoulderArm.transform.rotation = CountQuaternion(body.JointOrientations[JointType.ElbowLeft])
            * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-135, new Vector3(0, 0, 1));

        LeftForearm.transform.rotation = CountQuaternion(body.JointOrientations[JointType.WristLeft])
            * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-135, new Vector3(0, 0, 1));

        LeftWrist.transform.rotation = CountQuaternion(body.JointOrientations[JointType.HandLeft])
            * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));

        LeftIndexFinger.transform.rotation = CountQuaternion(body.JointOrientations[JointType.HandTipLeft])
            * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
        //LeftIndexFinger.transform.position = CountPosition(body.Joints[JointType.HandTipLeft]);

        #endregion

        #region Right arm

        RightShoulderArm.transform.rotation = CountQuaternion(body.JointOrientations[JointType.ElbowRight])
            * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(45, new Vector3(0, 0, 1));

        RightForearm.transform.rotation = CountQuaternion(body.JointOrientations[JointType.WristRight])
             * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        RightWrist.transform.rotation = CountQuaternion(body.JointOrientations[JointType.HandRight])
             * Quaternion.AngleAxis(0, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        ////RightIndexFinger.transform.rotation = CountQuaternion(body.JointOrientations[JointType.HandTipRight])
        ////     * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        #endregion

        #region Left leg

        LeftThigh.transform.rotation = CountQuaternion(body.JointOrientations[JointType.KneeLeft])
            * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        LeftKnee.transform.rotation = CountQuaternion(body.JointOrientations[JointType.AnkleLeft])
            * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        //LeftAnkle.transform.rotation = CountQuaternion(body.JointOrientations[JointType.AnkleLeft]);

        //LeftToe.transform.rotation = CountQuaternion(body.JointOrientations[JointType.FootLeft]);

        #endregion

        #region Right leg

        RightThigh.transform.rotation = CountQuaternion(body.JointOrientations[JointType.KneeRight])
            * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        RightKnee.transform.rotation = CountQuaternion(body.JointOrientations[JointType.AnkleRight])
            * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

        //RightAnkle.transform.rotation = CountQuaternion(body.JointOrientations[JointType.AnkleRight])
        //    * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));

        //RightToe.transform.rotation = CountQuaternion(body.JointOrientations[JointType.FootRight])
        //    * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));

        #endregion
    }

    private Quaternion CountQuaternion(JointOrientation orientation)
    {
        return new Quaternion(-orientation.Orientation.X, -orientation.Orientation.Y, orientation.Orientation.Z, orientation.Orientation.W);
    }

    private Vector3 CountPosition(Windows.Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X, joint.Position.Y + 1.5f, joint.Position.Z + 1);
    }

    // Inits default model state in Start() method
    private void SetDefaultModelState()
    {
        defaultRootPosition = Root.transform.position;

        int i = 0;
        defaultJointsState[i++] = Neck.transform.rotation;
        defaultJointsState[i++] = Head.transform.rotation;
        defaultJointsState[i++] = Hip.transform.rotation;
        defaultJointsState[i++] = LeftThigh.transform.rotation;
        defaultJointsState[i++] = LeftKnee.transform.rotation;
        defaultJointsState[i++] = LeftAnkle.transform.rotation;
        defaultJointsState[i++] = LeftToe.transform.rotation;
        defaultJointsState[i++] = RightThigh.transform.rotation;
        defaultJointsState[i++] = RightKnee.transform.rotation;
        defaultJointsState[i++] = RightAnkle.transform.rotation;
        defaultJointsState[i++] = RightToe.transform.rotation;
        defaultJointsState[i++] = Ribs.transform.rotation;
        defaultJointsState[i++] = LeftShoulderArm.transform.rotation;
        defaultJointsState[i++] = LeftUpperArm.transform.rotation;
        defaultJointsState[i++] = LeftForearm.transform.rotation;
        defaultJointsState[i++] = LeftWrist.transform.rotation;
        defaultJointsState[i++] = LeftIndexFinger.transform.rotation;
        defaultJointsState[i++] = RightShoulderArm.transform.rotation;
        defaultJointsState[i++] = RightUpperArm.transform.rotation;
        defaultJointsState[i++] = RightForearm.transform.rotation;
        defaultJointsState[i++] = RightWrist.transform.rotation;
        defaultJointsState[i++] = RightIndexFinger.transform.rotation;
    }

    // Resets current model state to default
    private void ResetCurrentModelState()
    {
        Root.transform.position = defaultRootPosition;

        int i = 0;
        Neck.transform.rotation = defaultJointsState[i++];
        Head.transform.rotation = defaultJointsState[i++];
        Hip.transform.rotation = defaultJointsState[i++];
        LeftThigh.transform.rotation = defaultJointsState[i++];
        LeftKnee.transform.rotation = defaultJointsState[i++];
        LeftAnkle.transform.rotation = defaultJointsState[i++];
        LeftToe.transform.rotation = defaultJointsState[i++];
        RightThigh.transform.rotation = defaultJointsState[i++];
        RightKnee.transform.rotation = defaultJointsState[i++];
        RightAnkle.transform.rotation = defaultJointsState[i++];
        RightToe.transform.rotation = defaultJointsState[i++];
        Ribs.transform.rotation = defaultJointsState[i++];
        LeftShoulderArm.transform.rotation = defaultJointsState[i++];
        LeftUpperArm.transform.rotation = defaultJointsState[i++];
        LeftForearm.transform.rotation = defaultJointsState[i++];
        LeftWrist.transform.rotation = defaultJointsState[i++];
        LeftIndexFinger.transform.rotation = defaultJointsState[i++];
        RightShoulderArm.transform.rotation = defaultJointsState[i++];
        RightUpperArm.transform.rotation = defaultJointsState[i++];
        RightForearm.transform.rotation = defaultJointsState[i++];
        RightWrist.transform.rotation = defaultJointsState[i++];
        RightIndexFinger.transform.rotation = defaultJointsState[i++];
    }
}

﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour {

    public GameObject palka;
    public GameObject montirovka;
    public GameObject light_saber;
    public GameObject axe;
    public GameObject minecraftSword;
    public GameObject wrench;

	// Use this for initialization
	void Start () {
        light_saber.SetActive(false);
        montirovka.SetActive(false);
        axe.SetActive(false);
        minecraftSword.SetActive(false);
        wrench.SetActive(false);
        gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
        //gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (EnemiesMoving.instance.points == 200
            || EnemiesMoving.instance.points == 400
            || EnemiesMoving.instance.points == 600
            || EnemiesMoving.instance.points == 800
            || EnemiesMoving.instance.points == 1000)
            gameObject.transform.position = new Vector3(-1.2f, 0.25f, 1.2f);
            //gameObject.SetActive(true);
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Left_Wrist_Joint_01")
        {
            if (EnemiesMoving.instance.points >= 200 && EnemiesMoving.instance.points < 400) 
            {
                palka.SetActive(false);
                wrench.SetActive(true);
                gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
                //gameObject.SetActive(false);
            }
            if (EnemiesMoving.instance.points >= 400 && EnemiesMoving.instance.points < 600)
            {
                wrench.SetActive(false);
                montirovka.SetActive(true);
                gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
                //gameObject.SetActive(false);
            }
            if (EnemiesMoving.instance.points >= 600 && EnemiesMoving.instance.points < 800)
            {
                montirovka.SetActive(false);
                minecraftSword.SetActive(true);
                gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
                //gameObject.SetActive(false);
            }
            if (EnemiesMoving.instance.points >= 800 && EnemiesMoving.instance.points < 1000)
            {
                minecraftSword.SetActive(false);
                axe.SetActive(true);
                gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
                //gameObject.SetActive(false);
            }
            if (EnemiesMoving.instance.points >= 1000)
            {
                axe.SetActive(false);
                light_saber.SetActive(true);
                gameObject.transform.position = new Vector3(-1.2f, -2, 1.2f);
                //gameObject.SetActive(false);
            }
        }
    }
}

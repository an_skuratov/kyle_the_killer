﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EffectsManager : MonoBehaviour {

    public Text scoreView;
    public GameObject soshnikovImage;
    public GameObject gameImage;
    public GameObject marioImage;
    public GameObject hlImage;
    public GameObject minecraftImage;
    public GameObject tfImage;
    public GameObject swImage;

    int curTheme;

	// Use this for initialization
	void Start () {
        curTheme = 0;
        soshnikovImage.SetActive(false);
        marioImage.SetActive(false);
        hlImage.SetActive(false);
        minecraftImage.SetActive(false);
        tfImage.SetActive(false);
        swImage.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        scoreView.text = "Score: " + EnemiesMoving.instance.points;
        if (EnemiesMoving.instance.points == 550)
            soshnikovImage.SetActive(false);

        switch (EnemiesMoving.instance.points)
        {
            case 200:
                if (curTheme == 200)
                    break;
                curTheme = 200;
                scoreView.GetComponent<AudioSource>().Stop();
                marioImage.SetActive(true);
                break;
            case 400: 
                if (curTheme == 400)
                    break;
                curTheme = 400;
                marioImage.SetActive(false);
                marioImage.GetComponent<AudioSource>().Stop();
                hlImage.SetActive(true);
                break;
            case 500:
                if (curTheme == 500)
                    break;
                curTheme = 500;
                soshnikovImage.SetActive(true);
                break;
            case 600: 
                if (curTheme == 600)
                    break;
                curTheme = 600;
                hlImage.SetActive(false);
                hlImage.GetComponent<AudioSource>().Stop();
                minecraftImage.SetActive(true);
                break;
            case 800:
                if (curTheme == 800)
                    break;
                curTheme = 800;
                minecraftImage.SetActive(false);
                minecraftImage.GetComponent<AudioSource>().Stop();
                tfImage.SetActive(true);
                break;
            case 1000:
                if (curTheme == 1000)
                    break;
                curTheme = 1000;
                tfImage.SetActive(false);
                tfImage.GetComponent<AudioSource>().Stop();
                swImage.SetActive(true);
                break;
        }
	}
}

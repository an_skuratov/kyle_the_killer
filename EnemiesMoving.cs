﻿using UnityEngine;
using System.Collections;

public class EnemiesMoving : MonoBehaviour {

    public static EnemiesMoving instance;

    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;

    public GameObject[] enemies;
    public int numberOfEnemies;
    public int points;
    private int[] stages;
    private float[] curRotation;
    private float[] hits;

    private float moveSpeed;
    private float rotationSpeed;

    private float timer;
    private float timerIncrement;

	// Use this for initialization
	void Start () {
        instance = this;

        enemy1.tag = "1";
        enemy2.tag = "2";
        enemy3.tag = "3";

        enemies = new GameObject[20];
        stages = new int[20];
        curRotation = new float[20];
        hits = new float[20];
        numberOfEnemies = 0;

        moveSpeed = 0.01f;
        rotationSpeed = 1;

        points = 0;
        timerIncrement = 7;
        timer = 0;

        for (int i = 0; i < 20; ++i)
            enemies[i] = null;

        // DELETE THIS CODE
        //enemies[numberOfEnemies] = Object.Instantiate(enemy1);
        //stages[numberOfEnemies] = 0;
        //curRotation[numberOfEnemies] = 0;
        //hits[numberOfEnemies] = 0;
        //numberOfEnemies++;
        //enemies[numberOfEnemies] = Object.Instantiate(enemy2);
        //stages[numberOfEnemies] = 0;
        //curRotation[numberOfEnemies] = 0;
        //hits[numberOfEnemies] = 0;
        //numberOfEnemies++;
        //enemies[numberOfEnemies] = Object.Instantiate(enemy3);
        //stages[numberOfEnemies] = 0;
        //curRotation[numberOfEnemies] = 0;
        //hits[numberOfEnemies] = 0;
        //numberOfEnemies++;
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < 20; ++i)
        {
            if (enemies[i] == null)
                continue;
            switch(enemies[i].tag)
            {
                case "1":
                    if (enemies[i].transform.position.x > -8 && stages[i] == 0)
                    {
                        enemies[i].transform.position += new Vector3(-moveSpeed, 0, 0);
                        break;
                    }
                    stages[i] = 1;
                    if (curRotation[i] < 90 && stages[i] == 1)
                    {
                        enemies[i].transform.Rotate(new Vector3(0, -rotationSpeed, 0));
                        curRotation[i] += rotationSpeed;
                        break;
                    }
                    stages[i] = 2;
                    if (enemies[i].transform.position.z > 7 && stages[i] == 2)
                    {
                        enemies[i].transform.position -= new Vector3(0, 0, moveSpeed);
                        break;
                    }
                    stages[i] = 3;
                    if (curRotation[i] < 135 && stages[i] == 3)
                    {
                        enemies[i].transform.Rotate(new Vector3(0, -rotationSpeed, 0));
                        curRotation[i] += rotationSpeed;
                        break;
                    }
                    stages[i] = 4;
                    if (enemies[i].transform.position.x < -1
                        && enemies[i].transform.position.z > 1.5
                        && stages[i] == 4)
                    {
                        enemies[i].transform.position += new Vector3(1.4f * moveSpeed, 0, 0);
                        enemies[i].transform.position += new Vector3(0, 0, -1.1f * moveSpeed);
                        break;
                    }
                    Destroy(enemies[i].GetComponent<Animator>());
                    stages[i] = 5;
                    if (hits[i] >= 1)
                        enemyKilled(enemies[i]);
                    break;
                
                case "2":
                    if (enemies[i].transform.position.x < 0 && stages[i] == 0)
                    {
                        enemies[i].transform.position += new Vector3(moveSpeed, 0, 0);
                        break;
                    }
                    stages[i] = 1;
                    if (curRotation[i] < 90 &&  stages[i] == 1)
                    {
                        enemies[i].transform.Rotate(new Vector3(0, rotationSpeed, 0));
                        curRotation[i] += rotationSpeed;
                        break;
                    }
                    stages[i] = 2;
                    if (enemies[i].transform.position.z > 1.4 && stages[i] == 2)
                    {
                        enemies[i].transform.position += new Vector3(0, 0, -1.5f*moveSpeed);
                        break;
                    }
                    Destroy(enemies[i].GetComponent<Animator>());
                    stages[i] = 3;
                    if (hits[i] >= 1)
                        enemyKilled(enemies[i]);
                    break;
                
                case "3":
                    if (enemies[i].transform.position.x > 8 && stages[i] == 0)
                    {
                        enemies[i].transform.position += new Vector3(-moveSpeed, 0, 0);
                        break;
                    }
                    stages[i] = 1;
                    if (curRotation[i] < 45 &&  stages[i] == 1)
                    {
                        enemies[i].transform.Rotate(new Vector3(0, -rotationSpeed, 0));
                        curRotation[i] += rotationSpeed;
                        break;
                    }
                    stages[i] = 2;
                    if (enemies[i].transform.position.x > 1.5 && enemies[i].transform.position.z > 1.5 && stages[i] == 2)
                    {
                        enemies[i].transform.position += new Vector3(-1.3f*moveSpeed, 0, -1.7f*moveSpeed);
                        break;
                    }
                    Destroy(enemies[i].GetComponent<Animator>());
                    stages[i] = 3;
                    if (hits[i] >= 1)
                        enemyKilled(enemies[i]);
                    break;

                default:
                    break;
            }
        }
        checkTimer();

        if (points > 600)
        {
            timerIncrement = 3.5f;
        }
	}

    private void enemyKilled(GameObject enemy)
    {
        Destroy(enemy);
        points += 25;
        numberOfEnemies--;
    }

    public void incrementHits(int enemyId)
    {
        hits[enemyId]++;
    }

    private void checkTimer()
    {
        if (timer <= 0)
        {
            createNewEnemy();
            timer += timerIncrement;
        }
        else
            timer -= Time.deltaTime;
    }

    private void createNewEnemy()
    {
        int enemyId = Random.Range(0, 3);
        switch (enemyId)
        {
            case 0:
                for (int i = 0; i < 20; i++)
                {
                    if (enemies[i] != null)
                        continue;
                    else 
                    {
                        enemies[i] = Object.Instantiate(enemy1);
                        stages[i] = 0;
                        curRotation[i] = 0;
                        hits[i] = 0;
                        numberOfEnemies++;
                        break;
                    }
                }
                break;

            case 1:
                for (int i = 0; i < 20; i++)
                {
                    if (enemies[i] != null)
                        continue;
                    else
                    {
                        enemies[i] = Object.Instantiate(enemy2);
                        stages[i] = 0;
                        curRotation[i] = 0;
                        hits[i] = 0;
                        numberOfEnemies++;
                        break;
                    }
                }
                break;

            case 2:
                for (int i = 0; i < 20; i++)
                {
                    if (enemies[i] != null)
                        continue;
                    else
                    {
                        enemies[i] = Object.Instantiate(enemy3);
                        stages[i] = 0;
                        curRotation[i] = 0;
                        hits[i] = 0;
                        numberOfEnemies++;
                        break;
                    }
                }
                break;

            default:
                break;
        }
    }
}

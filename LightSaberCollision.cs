﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class LightSaberCollision : MonoBehaviour {

    public static LightSaberCollision instance;

	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnCollisionEnter(Collision col)
    {
        for (int i = 0; i < EnemiesMoving.instance.numberOfEnemies; ++i)
        {
            if (EnemiesMoving.instance.enemies[i] == null)
                continue;
            if (EnemiesMoving.instance.enemies[i].name == col.gameObject.name)
            {
                EnemiesMoving.instance.incrementHits(i);
            }
        }
    }
}
